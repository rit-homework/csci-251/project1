using System;
using du;
using Xunit;

namespace Tests
{
    public class ResultTest
    {
        [Fact]
        public void TestFormatting()
        {
            var result = new Result(
                foldersCount: 19_260,
                filesCount: 86_801,
                totalBytes: 9_975_673_425,
                duration: TimeSpan.FromSeconds(1.2373179),
                resultType: Result.ResultType.Parallel
            );

            var formatted = result.Formatted();

            const string line1 = "Parallel Calculated in: 1.237s";
            const string line2 = "19,260 folders, 86,801 files, 9,975,673,425 bytes";
            
            Assert.Equal(line1 + '\n' + line2, formatted);
        }
    }
}