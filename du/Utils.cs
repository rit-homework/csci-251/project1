using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace du
{
    static class Utils
    {
        public static TimeSpan MeasureRuntime(Action inner)
        {
            var stopwatch = Stopwatch.StartNew();
            inner();
            stopwatch.Stop();

            return stopwatch.Elapsed;
        }

        private static void GetFiles(string path, Action<FileInfo> fileInfoDelegate)
        {
            string[] files;
            try
            {
                files = Directory.GetFiles(path);
            }
            catch (UnauthorizedAccessException)
            {
                return;
            }

            foreach (var filePath in files)
            {
                FileInfo info;
                try
                {
                    info = new FileInfo(filePath);
                }
                catch (UnauthorizedAccessException)
                {
                    continue;
                }

                fileInfoDelegate(info);
            }
        }

        private static void WithDirectories(string path, Action<string[]> withDirectories)
        {
            string[] directories;
            try
            {
                directories = Directory.GetDirectories(path);
            }
            catch (UnauthorizedAccessException)
            {
                return;
            }

            withDirectories(directories);
        }

        public static void Walk(string path, Action<FileInfo> fileInfoDelegate, Action folderDelegate)
        {
            GetFiles(path, fileInfoDelegate);
            WithDirectories(path, dirs =>
            {
                foreach (var directory in dirs)
                {
                    folderDelegate();
                    Walk(directory, fileInfoDelegate, folderDelegate);
                }
            });
        }

        public static void WalkParallel(string path, Action<FileInfo> fileInfoDelegate, Action folderDelegate)
        {
            GetFiles(path, fileInfoDelegate);
            WithDirectories(path, dirs =>
                Parallel.ForEach(dirs, directory =>
                {
                    folderDelegate();
                    WalkParallel(directory, fileInfoDelegate, folderDelegate);
                })
            );
        }
    }
}