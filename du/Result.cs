using System;
using System.Threading;

namespace du
{
    public class Result
    {
        public enum ResultType
        {
            Sequential,
            Parallel
        }

        private readonly int _foldersCount;
        private readonly int _filesCount;
        private readonly long _totalBytes;
        private readonly TimeSpan _duration;
        private readonly ResultType _type;

        public Result(int foldersCount, int filesCount, long totalBytes, TimeSpan duration, ResultType resultType)
        {
            this._foldersCount = foldersCount;
            this._filesCount = filesCount;
            this._totalBytes = totalBytes;
            this._duration = duration;
            this._type = resultType;
        }

        public string Formatted()
        {
            var line1 = string.Format(
                "{0} Calculated in: {1}s",
                _type.ToString(), _duration.TotalMilliseconds / 1000
            );
            
            var line2 = String.Format(
                "{0:n0} folders, {1:n0} files, {2:n0} bytes",
                _foldersCount, _filesCount, _totalBytes 
            );

            return line1 + '\n' + line2;
        }

        public static Result ComputeSequential(string path)
        {
            var foldersCount = 0;
            var filesCount = 0;
            long totalSize = 0;

            var runtime = Utils.MeasureRuntime(() => 
                Utils.Walk(path,
                    info => { totalSize += info.Length; filesCount++; },
                    () => foldersCount++
                )
            );

            return new Result(
                foldersCount, filesCount, totalSize, runtime, ResultType.Sequential
            );
        }

        public static Result ComputeParallel(string path)
        {
            var foldersCount = 0;
            var filesCount = 0;
            long totalBytes = 0;

            var runtime = Utils.MeasureRuntime(() => 
                Utils.WalkParallel(path,
                    info =>
                    {
                        Interlocked.Add(ref totalBytes, info.Length);
                        Interlocked.Increment(ref filesCount);
                    },
                    () => Interlocked.Increment(ref foldersCount)
                )
            );
            
            return new Result(
                foldersCount, filesCount, totalBytes, runtime, ResultType.Parallel
            );
        }
    }
}