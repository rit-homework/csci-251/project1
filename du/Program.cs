﻿using System;

namespace du
{
    internal static class Program
    {
        private static int Main(string[] arguments)
        {
            var args = ParseHandlingError(arguments);
            if (args == null)
            {
                return -1;
            }

            Console.WriteLine("Directory '{0}':", args.AbsolutePath);
            return ComputeHandlingError(args);
        }

        private static int ComputeHandlingError(Arguments args)
        {
            try
            {
                Compute(args);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return -1;
            }

            return 0;
        }

        private static void Compute(Arguments args)
        {
            switch (args.Mode)
            {
                case Arguments.RunningMode.Sequential:
                    Result.ComputeSequential(args.AbsolutePath).Print();
                    break;

                case Arguments.RunningMode.Parallel:
                    Result.ComputeParallel(args.AbsolutePath).Print();
                    break;

                case Arguments.RunningMode.Both:
                    Result.ComputeParallel(args.AbsolutePath).Print();
                    Result.ComputeSequential(args.AbsolutePath).Print();
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static Arguments ParseHandlingError(string[] args)
        {
            Arguments parsed;
            try
            {
                parsed = Arguments.Parse(args);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(Arguments.Usage);
                return null;
            }

            return parsed;
        }
    }

    public static class Extensions
    {
        public static void Print(this Result result)
        {
            string formatted = result.Formatted();
            Console.WriteLine();
            Console.WriteLine(formatted);
        }
    }
}