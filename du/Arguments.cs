using System;
using System.IO;
using System.Linq;

namespace du
{
    public class Arguments
    {
        public const string Usage = @"
Usage: du [-s] [-p] [-b] <path>
Summarize disk usage of the set of FILES, recursively for directories.

You MUST specify one of the parameters, -s, -p, or -b

-s    Run in single threaded mode
-p    Run in parallel mode (uses all available processors)
-b    Run in both parallel and single threded mode.
      Runs parallel followed by sequential mode.
";

        public RunningMode Mode { get; private set; }

        public string AbsolutePath { get; private set; }

        public enum RunningMode
        {
            Sequential,
            Parallel,
            Both
        }

        private static RunningMode? ParseMode(string s)
        {
            switch (s)
            {
                case "-s":
                    return RunningMode.Sequential;
                case "-p":
                    return RunningMode.Parallel;
                case "-b":
                    return RunningMode.Both;
                default:
                    return null;
            }
        }

        public static Arguments Parse(string[] args)
        {
            var flags = args.Where(s => s.StartsWith("-")).ToArray();
            Ensure(flags.Length <= 1, "Too many flags");
            Ensure(flags.Length != 0, "No flags provided");

            var mode = ParseMode(flags.First());
            Ensure(mode.HasValue, "Invalid flag provided");

            var paths = args.Where(s => !s.StartsWith("-")).ToArray();
            Ensure(paths.Length <= 1, "More than one path provided");
            Ensure(paths.Length != 0, "No path provided");

            var relativePath = paths.First();
            var absolutePath = Path.GetFullPath(relativePath);

            return new Arguments()
            {
                Mode = mode.Value,
                AbsolutePath = absolutePath,
            };
        }

        private static void Ensure(bool condition, string message)
        {
            if (!condition)
            {
                throw new ArgumentException(message);
            }
        }
    }
}